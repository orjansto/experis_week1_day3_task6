﻿using System;

namespace Task6
{
    class Program
    {

        /// <summary>
        /// Function for calculating and returning BMI.
        /// </summary>
        /// <param name="height">meters</param>
        /// <param name="weight">kg</param>
        /// <returns></returns>
        static double calcBMI(double height, double weight)
        {
            return weight / (height * height);
        }

        /// <summary>
        /// Function for checking the BMI and forwarding results to console
        /// </summary>
        /// <param name="BMI"></param>
        static void checkBMI(double BMI)
        {
            if (BMI < 18.5)
            {
                Console.WriteLine($"BMI is {BMI} and considered UNDERWEIGHT.");
            }
            else if (BMI < 24.9)
            {
                Console.WriteLine($"BMI is {BMI} and considered NORMAL WEIGHT.");
            }
            else if (BMI < 29.9)
            {
                Console.WriteLine($"BMI is {BMI} and considered OVERWEIGHT.");
            }
            else
            {
                Console.WriteLine($"BMI is {BMI} and considered OBESE.");
            }
        }

        /// <summary>
        /// Function will ask for height and weight, and display BMI in Console.
        /// </summary>
        static void askForBMI()
        {
            bool success;
            double height;
            double weight;
            double BMI;
            Console.WriteLine("This program will calculate BMI.");
            Console.WriteLine("Enter Height:");
            do
            {
                success = double.TryParse(Console.ReadLine().Trim(), out height);
                if (!success || height < 1 || height > 5)
                {
                    Console.WriteLine("Added height not valid, please try again:");
                }
            } while (!success || height < 1 || height > 5);
            Console.WriteLine("Enter Weight:");
            do
            {
                success = double.TryParse(Console.ReadLine().Trim(), out weight);
                if (!success || weight < 1 || weight > 1000)
                {
                    Console.WriteLine("Added weight not valid, please try again:");
                }
            } while (!success || weight < 1 || weight > 1000);
            BMI = calcBMI(height, weight);
            BMI = Math.Round(BMI, 2);
            checkBMI(BMI);
        }
        static void Main(string[] args)
        {
            
            Console.WriteLine("---Task 6---");
            askForBMI();
        }
    }
}
